# Introduction

* **Flask Journal App** is a Flask Powered Journal Creating App. This project is done as a fullfillment of Andela Kenya Bootcamp week 2 assesment.

* Click [here](https://journal-app-robert.herokuapp.com/) to see a working version.

* It has the following features;
  * User should be able to signup and login
  * User should be able to make a journal entry and save:
        - Date:
        - Journal (body):
        - Tags:
  * User should be able to edit/update old journals
  * User should be able to view all journal entries.
  * User should be able to search through all journals by key-words (tags) or text within the journal.
  * Journal listing are done in the order of journal journal date.

# Dependencies

# Back End Dependencies
* This app functionality depends on multiple python packages including;
  * Flask==0.11.1
  * Flask-Login==0.4.0
  * Flask-PageDown==0.2.2
  * Flask-SQLAlchemy==2.1
  * Flask-WTF==0.13.1
  * Jinja2==2.8
  * MarkupSafe==0.23
  * SQLAlchemy==1.1.3
  * WTForms==2.1
  * Werkzeug==0.11.11
  * click==6.6
  * gunicorn==19.6.0
  * itsdangerous==0.24
  * passlib==1.6.5

# Installation
To run this project, you'll need a working installation of python 3 and pip. Flask should also be installed.

## To install the app:
1. Clone this repository - https://github.com/rnjane/bc-14-journal-flask-app
2. Install requirements - pip install requirements.txt
3. Run app.py file.

# Workflow
The user can create and account and create journals. User then can view all journals and edit them.

# Icebox
The feature that will added in the days to come.
* Social Authentication implementaion.
* Integrate firebase with the application.
